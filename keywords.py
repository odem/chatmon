#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sqlite3
import threading

kw_table_sql = 'CREATE TABLE IF NOT EXISTS keywords (keyword text PRIMARY KEY)'
kw_insert_sql = 'INSERT OR IGNORE INTO keywords(keyword) VALUES (?)'
kw_select_sql = 'SELECT * FROM keywords ORDER BY keyword'
kw_delete_sql = 'DELETE FROM keywords WHERE keyword = ?'
kw_truncate_sql = 'DELETE FROM keywords'


class Keywords(object):
    def __init__(self, filename):
        self.lock = threading.Lock()
        self.con = sqlite3.connect(filename, check_same_thread=False)
        self.cur = self.con.cursor()
        self.cur.execute(kw_table_sql)
        self.cache = set(self.get_keywords())

    def add_keywords(self, keywords):
        self.lock.acquire()
        self.cache.update(keywords)
        self.cur.executemany(kw_insert_sql, self._list_generator(keywords))
        self.con.commit()
        self.lock.release()

    def get_keywords(self):
        self.lock.acquire()
        for row in self.cur.execute(kw_select_sql):
            yield row[0]
        self.lock.release()

    def del_keywords(self, keywords):
        self.lock.acquire()
        self.cache.difference_update(keywords)
        self.cur.executemany(kw_delete_sql, self._list_generator(keywords))
        self.con.commit()
        self.lock.release()

    def _list_generator(self, lst):
        for e in lst:
            yield (e,)
