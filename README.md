### Installation ###

* Install telegram-cli
* Run it in json mode (optionaly in daemon mode): `./bin/telegram-cli --json -P 4458`
* Install dependencies from requirements.txt or setup.py
* Make a copy of sample config: `cp config-sample.py config.py`
* Edit it
* Run: `python chatmon.py`
