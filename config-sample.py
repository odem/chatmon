TG_HOST = 'localhost'
TG_PORT = 4458

CONTROL_GROUP = 1126772323
LOG_GROUP = 1130343967
# id of log channel as returned by dialog_list
LOG_GROUP_ID = u'$050000001fae5f43a3813f9d7d1d78af'

DATA_DIR = 'data'
KEYWORDS_FILE = DATA_DIR + '/keywords'

SEND_TRACEBACK = True
