#!/usr/bin/env python
# -*- coding: utf-8 -*-

import config as cfg

from pytg.receiver import Receiver
from pytg.sender import Sender
from pytg.utils import coroutine

import re
import pprint
import os
import errno
import sys
import traceback
import logging

from keywords import Keywords

log_format = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
logging.basicConfig(format=log_format, level=logging.INFO)
logger = logging.getLogger(__name__)

kw = Keywords(cfg.KEYWORDS_FILE)


# Handles messages from receiver
@coroutine
def message_loop(sender):
    try:
        while True:
            msg = (yield)

            #pprint.pprint(msg)

            if msg.event != "message" or getattr(msg, 'text', None) is None:
                continue

            #print(msg.text)

            rcv = getattr(msg, 'receiver', None)
            sender.mark_read(rcv.id)

            # skip messages in LOG channel
            if getattr(rcv, 'peer_id', None) == cfg.LOG_GROUP:
                continue

            if getattr(rcv, 'peer_id', None) == cfg.CONTROL_GROUP:
                handle_control(sender, msg)
                continue

            if msg.own:
                continue

            handle_text(sender, msg)
    except KeyboardInterrupt:
        logger.info("Exiting on KeyboardInterrupt")


# Checks text messages by regular expressions
def handle_text(sender, msg):
    try:
        for word in kw.cache:
            if re.search(word, msg.text, re.IGNORECASE | re.UNICODE):
                sender.send_text(cfg.LOG_GROUP_ID,
                                 'Triggered on %s from %s' %
                                 (word, username_or_title(sender, msg)))
                sender.fwd(cfg.LOG_GROUP_ID, msg.id)
                return
    except AttributeError:
        # most likely that it's a private message, not group
        # this messages have no title attribute in msg.receiver
        # ignore such messages
        pass
    except:
        pprint.pprint(msg)
        tb = traceback.format_exc()
        logger.error(tb)
        if cfg.SEND_TRACEBACK:
            send_html(sender, cfg.LOG_GROUP_ID, '<code>' + tb + '</code>')


# /add command handler
def add_keyword(sender, msg, keyword):
    try:
        re.compile(keyword)
    except:
        sender.send_text(msg.receiver.id, u'Syntax error.')
        return

    kw.add_keywords([keyword])
    sender.send_text(msg.receiver.id, u'Keyword added.')


# /del command handler
def del_keyword(sender, msg, keyword):
    if keyword in kw.cache:
        kw.del_keywords([keyword])
        sender.send_text(msg.receiver.id, u'Keyword removed.')
    else:
        sender.send_text(msg.receiver.id, u'Keyword not found.')


# /clear command handler
def clear(sender, msg):
    kw.del_keywords(kw.cache)
    sender.send_text(msg.receiver.id, u'Keywords removed.')


# /list command handler
def list_keywords(sender, msg):
    words = '\n'.join(kw.get_keywords())
    if len(words) == 0:
        text = u'There is no keywords.'
        sender.send_text(msg.receiver.id, text)
    else:
        sender.send_text(msg.receiver.id, words)


# /b command handler
def broadcast(sender, msg, text):
    dialogs = sender.dialog_list()
    exclude = [cfg.CONTROL_GROUP]
    for d in dialogs:
        try:
            if d.peer_id not in exclude and d.peer_type in ['channel', 'chat']:
                send_html(sender, d.id, text)
        except:
            pprint.pprint(msg)
            tb = traceback.format_exc()
            logger.error(tb)
            if cfg.SEND_TRACEBACK:
                send_html(sender, cfg.LOG_GROUP_ID, '<code>' + tb + '</code>')


# /format command handler
def format_message(sender, msg, text):
    send_html(sender, msg.receiver.id, text)


# /join command handler
def join_chat(sender, msg, link):
    try:
        if link[0] == '@':
            # need to resolve username before joining
            name = link[1:]
            res = sender.resolve_username(name)
            pprint.pprint(res)
            sender.channel_join(res.id)
            send_html(sender, msg.receiver.id,
                      'id: <code>%s</code><br>peer_id: <code>%s</code>' %
                      (res.id, res.peer_id))
        else:
            sender.import_chat_link(link)
    except:
        traceback.print_exc(file=sys.stdout)


help_string = u'''
/add <regexp> adds regular expression to match keywords
/del <regexp> removes regexp
/clear removes all keywords
/list lists currently used regexps
/b <message> sends message to all channels
/format <message> format message (for debug)
/join <link> joins chat by given link
'''


# /help command handler
def help_command(sender, msg):
    sender.send_text(msg.receiver.id, help_string)


# Dictionary of command handlers
cmd_handlers = {
    'add': add_keyword,
    'del': del_keyword,
    'clear': clear,
    'list': list_keywords,
    'help': help_command,
    'b': broadcast,
    'format': format_message,
    'join': join_chat,
}


# Control messages dispatcher
def handle_control(sender, msg):
    try:
        m = re.match(r'/(\w+) (.+)', msg.text)
        cmd = cmd_handlers[m.group(1)]
        cmd(sender, msg, m.group(2))
    except:
        try:
            m = re.match(r'/(\w+)', msg.text)
            cmd = cmd_handlers[m.group(1)]
            cmd(sender, msg)
        except:
            pass


# Returns channel username (like @p1zda) or title
def username_or_title(sender, msg):
    try:
        info = sender.channel_info(msg.receiver.id)
        return '@' + info.username
    except:
        return msg.receiver.title


# Sends HTML-formated message
def send_html(sender, chat_id, html):
    sender.raw('[html] msg ' + chat_id + ' ' + html)


def main():
    try:
        os.makedirs(cfg.DATA_DIR)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise

    receiver = Receiver(host=cfg.TG_HOST, port=cfg.TG_PORT)
    sender = Sender(host=cfg.TG_HOST, port=cfg.TG_PORT)
    receiver.start()
    receiver.message(message_loop(sender))
    receiver.stop()


if __name__ == '__main__':
    main()
